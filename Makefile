COMPILER = xelatex

.PHONY: all pdf

all: pdf

pdf: *.tex
	${COMPILER} $^

release:
	cp *.pdf output/

clean: 
	rm -f *.aux *.log *.out *.toc

distclean: clean
	rm -f *.pdf
