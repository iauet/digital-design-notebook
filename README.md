# Digital Systems Design Notebook

This is a collaborative notebook for **digital systems design** course held by
Soodeh Bakhshandeh.

Providing an open source environment is expected to motivate students to
participate in development of this notebook.
In contrast, fresh year studets are promoted to use an up to date notebook.

## Requirements

If you are a collaborator, then you need a few tools to make your tex files
compile properly:

* texlive-core texlive-latexextra texlive-langextra texlive-formatextra

## Compile

```sh
xelatex digital-design.tex
```

Running this command will give you the *digital-design.pdf* file if no errors
are found.

## Collaborators

1. [Brian Salehi](https://github.com/briansalehi)
